import type { PointronEventEnum } from "./pointronEvent.enum"

export type PointronEvent = {
    event: PointronEventEnum,
    value?: boolean | string | undefined
}