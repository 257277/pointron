import type { PageMenuItem } from "$lib/tidy/types/pagemenuitem.type";

export type PointronConstants = {
  timerModes: string[];
  focusPlaceholderText: string[];
  runningOutDuration: number;
  gapThreshold: number;
};
